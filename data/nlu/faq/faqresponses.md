###### BASIC FAQ ######
###### BASIC - ABOUT JAVA ######

## faq - What is Java
* basic_faq/what-is-java
	- Java is a write-once, run-anywhere programming language developed by Sun Microsystems. It was designed to have the “look and feel” of the C++ programming language, but it is simpler to use than C++ and enforces an object-oriented programming model.

## faq - When was Java developed
* basic_faq/when-was-java-developed
	- Java was first released in 1995\n\nJava was originally developed by James Gosling at Sun Microsystems (which has since been acquired by Oracle) and released in 1995 as a core component of Sun Microsystems' Java platform.

## faq - How is Java different from other languages
* basic_faq/how-is-java-different
	- The main difference between Java and any other programming language is the unique method in which Java code is executed. Unlike compiled languages such as C++, Java is compiled into bytecode which can run on any device with the Java Virtual Machine (JVM). C++, on the other hand, is compiled directly into machine code and therefore, can only run on the same platform in which it was compiled.

## faq - What is JavaScript
* basic_faq/javascript
	- Javascript and Java are to totally different things. Javascript has nothing to do with Java and its behaviour is not answerd in that bot. Unfortunately, the naming of both programming languages has been unfavourable.

## faq - What is Java used for
* basic_faq/what-is-java-for
	- What Do Java Programmers Do? They write programs. Java is a general purpose Programming Language and have a wide range of applications.

## faq - What is JDK
* basic_faq/what-is-jdk
	- JDK stands for for Java Development Kit. It is a software development environment used for developing Java applications and applets. It includes the Java Runtime Environment (JRE), an interpreter/loader (Java), a compiler (javac), an archiver (jar), a documentation generator (Javadoc) and other tools needed in Java development. It is the tool necessary to compile, document and package Java programs. 

## faq - What is JRE
* basic_faq/what-is-jre
	- JRE stands for for Java Runtime Environment. JRE refers to a runtime environment in which Java bytecode can be executed. It’s an implementation of the JVM which physically exists.

## faq - What is JVM
* basic_faq/what-is-jvm
	- JVM stands for for Java Virtual Machine. Java Virtual Machine is an engine that provides runtime environment to drive the Java Code or applications. It converts Java bytecode into machines language. JVM is a part of Java Run Environment (JRE). In other programming languages, the compiler produces machine code for a particular system. However, Java compiler produces code for a Virtual Machine known as Java Virtual Machine.

## faq - JDK vs. JVM vs. JRE
* basic_faq/jvm-jdk-jre
	- The full form of JDK is Java Development Kit, while the full form of JRE is Java Runtime Environment, while the full form of JVM is Java Virtual Machine. \n\nJDK is a software development kit whereas JRE is a software bundle that allows Java program to run, whereas JVM is an environment for executing bytecode.

## faq - Compilation process in Java
* basic_faq/compilation-process-in-java
	- Java code needs to be compiled twice in order to be executed:\n1. Java programs need to be compiled to bytecode.\n2. When the bytecode is run, it needs to be converted to machine code.\n\nThe Java classes/bytecode are compiled to machine code and loaded into memory by the JVM when needed the first time. This is different from other languages like C/C++ where programs are to be compiled to machine code and linked to create an executable file before it can be executed.

## faq - What is an algorithm
* basic_faq/what-is-an-algorithm
	- An algorithm is an unambiguous step-by-step procedure that accomplishes a task in a finite period of time. Algorithms are defined by the specific steps to achieving a goal, not by the languages they're implemented in.


###### BASIC - JAVA COMMENTS ######

## faq - use comments in java
* basic_faq/comments
	- You can comment single java lines by using // at the beginning. Code being behind // will not get executed.\n\nMulti-line comments start with /* and  ends with */. Any text between /* and */ will be ignored by Java.


##### BASIC - BASIC FUNCTIONS ######

## faq - print a line
* basic_faq/print
	- The function System.out.println(); prints information to your terminal/console. It takes all sorts of arguments, for example numbers, strings or booleans.

## faq - use input arguments
* basic_faq/use-input-arguments
	- If you run a java file from terminal, you can add arguments to it, for example: java filename arg1 arg2. Those arguments will directly be accessable inside the main method by using the args-array. arg1 is therefore arg[0]. Make sure to parse it correctly, all arguments are Strings by default.

## faq - import libraries
* basic_faq/import-library
	- You can import libraries by using import <path> with path being the relative path of the library to your project.

## faq - is there goto in java
* basic_faq/goto
	- There is no goto in Java. Goto is evil. Don't even think about it. Thank you.




###### BASIC - VARIABLES ######

## faq - What is a variable
* basic_faq/what-is-a-variable
	- A Java variable is a piece of memory that can contain a data value. Variables are typically used to store information which your Java program needs to do its job. A variable thus has a data type.\n\nIf you want to learn more about data types in Java, ask me 'What data types does Java have?'.

## faq - How to declare a variable
* basic_faq/how-to-declare-a-variable
	- To declare (create) a variable, you will specify the type, leave at least one space, then the name for the variable and end of the line with a semicolon (;) \n\nHere is an example declaration of a variable called score. \nint score; \n\nHere is an example that shows declaring a variable and initializing it all in a single statement: \nint score = 4;

## faq - How to initialise a variable
* basic_faq/how-to-initialize-a-variable
	- Declaration is the process of defining the variable along with its type and name. \n\nHere, we're declaring a variable called score: \nint id; \n\nInitialization, on the other hand, is all about assigning a value. For example: \nscore = 1; \n\nWe can also declare and initialize a variable in a single statement: \nint score = 4;

## faq - local and global variables
* basic_faq/global-vs-local-variables
	- A Local Variable is a type of variable declared within programming block. It can only be used inside the code block in which it is declared. The local variable exists until the block of the function is under execution. After that, it will be destroyed automatically. \n\nA Global Variable in the program is a variable defined outside the subroutine or function. It has a global scope means it holds its value throughout the lifetime of the program. Hence, it can be accessed throughout the program by any function defined within the program, unless it is shadowed. \n\nint a=4;\npublic int add(){\n\nint b=5;\nreturn a+b;\n}\nIn this example 'a' is a global variable and 'b' is a local variable.

## faq - final variable keyword
* basic_faq/final
	- A final value is a value which cannot be changed after initialising. Final methods are methods which cannot be overridden. Example:\n public final int klinger = 10; //I will stay at 10 til the end of the universe.

## faq - Static variables
* basic_faq/static-variables
	- A static variable is a value of a class which does not change for every object of the class. Changing the static value for one object changes the value for all objects. Static variables are, essentially, global variables.

## faq - what is null
* basic_faq/what-is-null
	- Null refers to a null reference which means that a value or a object is not defined yet.



###### BASIC - PRIMITIVE DATA TYPES ######

## faq - Java data types 
* basic_faq/data-types
	- Data types are divided into two groups:\n\nPrimitive data types include byte, short, int, long, float, double, boolean and char\n\nNon-primitive data types such as String, Arrays and Classes\n\nYou can ask me about each particular data type and I'll tell you more about it.

## faq - Byte in Java
* basic_faq/byte-data-type
	- 'byte' is a primitive data type in Java.\n\nThe byte data type can store whole numbers from -128 to 127. This can be used instead of int or other integer types to save memory when you are certain that the value will be within -128 and 127 (8 bits).

## faq - Short in Java
* basic_faq/short-data-type
	- 'short' is a primitive data type in Java.\n\nThe short data type can store whole numbers from -32768 to 32767 (16 bits).

## faq - Int in Java
* basic_faq/int-data-type
	- 'int' is a primitive data type in Java.\n\nThe int data type can store whole numbers from -2147483648 to 2147483647 (32 bits). In general, the int data type is the preferred data type when we create variables with a numeric value.

## faq - Long in Java
* basic_faq/long-data-type
	- 'long' is a primitive data type in Java. \n\nThe long data type can store whole numbers from -2^63 to 2^63 (64 bits). This is used when int is not large enough to store the value. Note that you should end the value with an "L".

## faq - Float in Java
* basic_faq/float-data-type
	- 'float' is a primitive data type in Java.\n\nThe float data type can store fractional numbers up to 7 decimal digits. The number can be from 3.4e−038 to 3.4e+038 (32 bits). Note that you should end the value with an "f".

## faq - Double in Java
* basic_faq/double-data-type
	- 'double' is a primitive data type in Java.\n\nThe double data type can store fractional numbers up to 16 decimal digits. The number can be from 1.7e−308 to 1.7e+308. Note that you should end the value with a "d".\n\nA floating point number can also be a scientific number with an "e" to indicate the power of 10.

## faq - Boolean in Java
* basic_faq/boolean-data-type
	- 'boolean' is a primitive data type in Java.\n\nA boolean data type is declared with the boolean keyword and can only take the values true or false (1 bit).

## faq - Char in Java
* basic_faq/char-data-type
	- 'char' is a primitive data type in Java.\n\nThe char data type is used to store a single character. The character must be surrounded by single quotes, like 'A' or 'c'. A string internally consists of chars.

## faq - Difference int and long
* basic_faq/difference-int-long
	- An integer fits 32bits of data, while a long has double the size. Therefore, integers can fit numbers between -2^31 to 2^31, while long numbers can be in a range of [-2^63,2^63].

## faq - Difference double and float
* basic_faq/difference-double-float
	- Both floats and doubles are floating numbers, but a double takes double the size and can therefore fit double the precision of a float.



###### BASIC - TYPE CASTING ######

## faq - What is casting
* basic_faq/what-is-casting
	- All casting really means is taking an Object of one particular type and “turning it into” another Object type. This process is called casting a variable. This topic is not specific to Java, as many other programming languages support casting of their variable types. But, as with other languages, in Java you cannot cast any variable to any random type.\n\nTo know more you can ask me 'What are the rules of casting in Java?'.

## faq - Cast double to int
* basic_faq/cast-double-int
	- A double d gets casted to an integer by using the following: int a = (int) d;



##### BASIC - ACCESS MODIFIERS ######

## faq - Access modifiers
* basic_faq/access-modifiers
	- Java provides a number of access modifiers to set access levels for classes, variables, methods, and constructors. The four access levels are: private, package-private, protected and public. Private values/methods are only visible within the class itself.\n\nPackage-private values/methods are visible within all classes being inside the package of the original class. Proteced does also include the possibility of accessing data through subclasses. Variables and methods declared as public are accessable overall.\n\n\n\nYou can ask me about a specific modifier by writing: 'What does a private modifier do?'.

## faq - Private modifiers
* basic_faq/private-modifier
	- 'private' is an access modifier. This modifier tells that the field is accessible only within its own class. A class, method, constructor, interface, etc. declared public can be accessed from any other class. Therefore, fields, methods, blocks declared inside a public class can be accessed from any class belonging to the Java Universe.\n\nIn total Java includes 4 access modifiers (private, public, protected, package-protected).

## faq - Public modifiers
* basic_faq/public-modifier
	- 'public' is an access modifier. This modifier tells that the field is accessible from all classes.\n\nIn total Java includes 4 access modifiers (private, public, protected, package-protected).

## faq - Protected modifiers
* basic_faq/protected-modifier
	- 'protected' is an access modifier. These Variables, methods, and constructors, which are declared protected in a superclass can be accessed only by the subclasses in other package or any class within the package of the protected members' class. The protected access modifier cannot be applied to class and interfaces. Methods, fields can be declared protected, however methods and fields in a interface cannot be declared protected.\n\nIn total Java includes 4 access modifiers (private, public, protected, package-protected).

## faq - Package-protected modifier
* basic_faq/package-protected-modifier
	- 'package-protected' is an access modifier. This modifier tells that values/methods are visible within all classes being inside the package of the original class.

## faq - What does static mean
* basic_faq/static-keyword
	- static is a non-access modifier in Java which is applicable variables and methods. You can ask me more about 'static methods' or 'static variables'.



###### BASIC - CONDITIONALS ######

## faq - Conditional statements
* basic_faq/conditional-statements
	- Conditional statements give us this ability to check conditions and react accordingly.\n\nThe simplest conditional statement in Java is the 'if' statement.\n\nA second form of conditional statement has two possibilities, indicated by 'if' and 'else'. The possibilities are called branches, and the condition determines which one gets executed.\n\nTo implement more complicated logic you can use a series of 'if-else'. This is called Chaining and Nesting.\n\nAnother alternative to implement conditional logic is to use 'switch' statement.

## faq - If-else condition
* basic_faq/how-to-write-if-else
	- Use the if statement to specify a block of Java code to be executed if a condition is true.

## faq - usage of switch-case in java
* basic_faq/switch-case
	- Switch-case is an alternative way of defining conditions, in particular used if there are many conditions at once. For more information on how to use them, have a look for this wonderful manual on the Internet: [Switch statement in Java](https://www.tutorialspoint.com/java/switch_statement_in_java.htm)

## faq - Ternary operator? : ?
* basic_faq/ternary-operator
	- The ternary operator is some sort of shortcut for an if-else-condition. The operator is very short and effective, but makes understanding your code a bit more difficult. For more information on how to use it, have a look at this website: [Java ternary operator with examples](https://www.geeksforgeeks.org/java-ternary-operator-with-examples/)

## faq - De Morgan's law
* basic_faq/de-morgans-law
	- De Morgan’s laws can help you to negate an expression that contains logical operators:\n\n!(A && B)  is the same as !A || !B\n\n!(A || B) is the same as !A && !B 

## faq - Flag variables
* basic_faq/flag-variables
	- A flag variable, in its simplest form, is a variable you define to have one value until some condition is true, in which case you change the variable's value. It is a variable you can use to control the flow of a function or statement, allowing you to check for certain conditions while your function progresses.

## faq - Combine booleans
* basic_faq/combine-booleans
	- Booleans are primitive datatypes and therefore get compared by using the == sign. You can concatinate two boolean expressions by || and && for or and and. Example:\n\n boolean a = false;\n boolean b = true;if(a==5 && b==6) {\n \tSystem.out.println("a is 5 and b is 6");\n }



###### BASIC - CONTROL FLOW ######

## faq - Control Flow statements
* basic_faq/control-flow-statements
	- There are 3 types of control flow statements supported by the Java programming language.\n\nDecision-making statements : if-then, if-then-else, switch\n\nLooping statements : for, while, do-while\n\nBranching statements : break, continue, return

## faq - Continue statement
* basic_faq/continue
	- Analogue to the break function, you can skip the current iteration of a loop by using continue. Break will break the entire loop, while continue only breaks the current iteration.

## faq - Break a loop
* basic_faq/break-loop
	- By using the operator break; you can immediately stop a loop from running, going to the next code block behind the for-loop.



###### BASIC - JAVA OPERATORS ######

## faq - Java operators
* basic_faq/java-operators
	- Operators are used to perform operations on variables and values. Java divides the operators into the following groups:\n\nArithmetic operators\nAssignment operators\nComparison operators\nLogical operators\nBitwise operators

## faq - Arithmetic operators
* basic_faq/arithmetic-operators
	- Arithmetic operators are used to perform common mathematical operations. There are 7 arithmetic operators in Java: addition (+), subtraction (-), multiplication (*), division (/), modulus (%), increment (++), decrement (--).

## faq - Assignment operators
* basic_faq/assignment-operators
	- Assignment operators are used to assign values to variables. We use the assignment operator (=) to assign the value 10 to a variable called x:\nint x = 10;\n\nThere are other assignment operators that are combiinations of a arithmetic (+, -, /, *, %, ) operator and assignment operator. For examle The addition assignment operator (+=) adds a value to a variable:\n int x = 10;\nx += 5;

## faq - Comparison operators
* basic_faq/comparison-operators
	- Comparison operators are used to compare two values. There are 7 comparison operators in Java: ==, !=, >, <, >=, <=.

## faq - Logical operators
* basic_faq/logical-operators
	- Java has three logical operators: &&, ||, and !, which respectively stand for and, or, and not. Logical operators are mostly used in conditional statements because they return a boolean value.

## faq - Bitwise operators
* basic_faq/bitwise-operators
	- Java has three logical operators: &&, ||, and !, which respectively stand for and, or, and not. Logical operators are mostly used in conditional statements because they return a boolean value.

## faq - how to shift bits
* basic_faq/int-bitshift
	- You can use bitshifting by using the >> or << operator. Bitshifting an integer by one bit is similar to multiplying or dividing by 2.

## faq - Modulo
* basic_faq/modulo
	- The modulo operator % returns the remainder of a division in java, for example: int a = 7%5; will return 2.

## faq - difference compare and declare = and ==
* basic_faq/difference-compare-declare
	- One equals sign declares the value of a variable, while == compares two values. The comparation operator will therefore return a boolean.

## faq - Comparing Integers
* basic_faq/compare-integer
	- Integers are primitive datatypes and therefore get compared by using the == sign. Example:\n int a = 5;\n int b = 4;if(a==b) {\n \tSystem.out.println("The integers are identical.");\n }

## faq - equals
* basic_faq/equals
	- The equals function is a function to compare if the values of two objects are the same. Its the same as == for primitive data types. You use equals to compare two strings for example.



###### BASIC - JAVA ARRAYS ######

## faq - What is an array
* basic_faq/what-is-array
	- In computer programming, an array is a collection of similar types of data. For example, if we want to store the names of 100 people then we can create an array of the string type that can store 100 names.\n\nString[] names = new String[100];\n\nYou can ask me how to declare an array if you are not still not sure how to use them.

## faq - Declaration array
* basic_faq/declare-array
	- The declaration of an array can be done in multiple ways.\n\n int[] numbers = {3, 4, 12}; //elements already known\n int[] numbers = new int[3]; //declaring the fixed size, but no element yet

## faq - Array size
* basic_faq/array-size
	- You can get the size of an array arr by using arr.length.

## faq - Resize Array
* basic_faq/resize-an-array
	- You can't resize an array in Java. You will need to use another array of a different size and copy all the elements there. Another option is to use an ArrayList which does this for you when you need to make the array bigger.

## faq - iterate over array
* basic_faq/iterate-over-array
	- You can iterate over all array elements in many ways, the easiest one being a for-loop from 0 to the length of an array, accessing every array element with the run variable. You can also use a "for each" loop for that. Here's an example:\n for (int i: input) { //do something with i }

## faq - How to access array element
* basic_faq/access-array-element
	- Accessing an array element is done by using squared brackets. Make sure that you access an element which is not out of bounds compared to the array size. Example:\n\n int[] numbers = {3, 4, 12};\n int thirdElement = numbers[2];

## faq - get last element of array
* basic_faq/last-element
- The last element of an array a is a[a.length-1].

## faq - Array indexes
* basic_faq/array-indexes
	- Array in Java is index-based. Array indexes start with 0: [0] is the first element. [1] is the second element, etc.

## faq - min and max of array
* basic_faq/min-max-array
	- You can get the minimum or maximum number of an array by using Math.min() and Math.max().

## faq - copy array values into new array
* basic_faq/copy-an-array
	- You need to make a new array of the same size and copy each value at each array position into the new array by using a for-loop.

## faq - reverse an array
* basic_faq/reverse-an-array
	- ArrayUtils.reverse(array); reverses an array. Make sure to import org.apache.commons.lang3.ArrayUtils. Alternatively, make a new array and iterate over it in a for-loop starting from last element.

## faq - Add element to array
* basic_faq/add-array-element
	- There is no direct way to add elements from an Array in Java. Though Array in Java objects, it doesn't provide any methods to add(), remove(). This is the reason Collection classes like ArrayList and HashSet are very popular. You can use add() method in ArrayList like this:\n\n\n\nArrayList<Integer> values = new ArrayList<Integer>();\nvalues.add(1);

## faq - Remove element from array
* basic_faq/remove-array-element
	- There is no direct way to remove elements from an Array in Java. Though Array in Java objects, it doesn't provide any methods to add(), remove(). This is the reason Collection classes like ArrayList and HashSet are very popular. You can use remove() method in ArrayList like this:\n\nArrayList<Integer> values = new ArrayList<Integer>();\nvalues.add(1);\nvalues.remove(1);

## faq - Array List
* basic_faq/arraylist
	- An arrayList has similar functionality as an array. The main difference is that arrays have a fixed size, while arraylists are dynamic sized. You can initialise an arraylist for integers with the following command: ArrayList<Integer> values = new ArrayList<>();

## faq - difference arrayList and array
* basic_faq/difference-ArrayList-Array
	- An array is basic functionality provided by Java. ArrayList is part of collection framework in Java. Therefore array members are accessed using [], while ArrayList has a set of methods to access elements and modify them.\n\nAn arrayList has similar functionality as an array. The main difference is that arrays have a fixed size, while arraylists are dynamic sized. You can initialise an arraylist for integers with the following command: ArrayList<Integer> values = new ArrayList<>();

## faq - concatinate two arrays
* basic_faq/concatinate-arrays
	- There is no easy way of concatinating two arrays. The best idea is to initialize a new array having the size of the two old arrays together and then fill the new array with the data of the old ones by using a for-loop.

## faq - get index of an array element
* basic_faq/get-index-of-array-element
	- If you are using ArrayList, you can simply use indexOf() function to get the index of an array element.\n\nIf not, you will need to iterate over a java array and compare your element to other array elements. This is called linear search.

## faq - generate a twodimensional array
* basic_faq/2D-array
	- You can use [] multiple times. int[][] = new int[3][3]; provides you with a 3x3 matrix.



###### BASIC - JAVA STRINGS ######

## faq -  What is a string?
* basic_faq/what-is-a-string
	- Strings, which are widely used in Java programming, are a sequence of characters. In the Java programming language, strings are objects. The Java platform provides the String class to create and manipulate strings.

## faq - Create new string
* basic_faq/create-a-string
	- You can define a new string with quotation marks around the text. Example: String s = "Hello World";

## faq - String length
* basic_faq/string-length
	- The size of a string s in single characters is available by using s.size().

## faq - use of toString method
* basic_faq/toString-method
	- The toString() method is a method which you can implement inside your own java class defining how a class should appear on terminal if you print the class to it. For more information on how to use them, have a look for this wonderful manual on the Internet: [Understanding toString() method](https://www.javatpoint.com/understanding-toString()-method)

## faq - get letter in string by index
* basic_faq/get-letter-in-string-by-index
	- You can get the character of position x in the string s by using the command: char c = s.charAt(x);

## faq - how to concatinate strings
* basic_faq/concatinate-strings
	- You can put two strings together with the + operator. Example: String new = old1 + old2; with old1 and old2 being strings.

## faq - split a string
* basic_faq/split-string
	- The method s.split(" ") splits a string s at every space character. It returns an array of substrings.

## faq - get new line in java string
* basic_faq/new-line
	- You can use "\n" inside a string to get into a new line.

## faq - Compare strings
* basic_faq/compare-strings
	- Strings in Java are objects, not primitive datatypes. This means, that they get compared by using the method "equals()".\n Example: String a = "Klinger";\n String b = "Napoleon"\n if(a.equals(b)) {\n \tSystem.out.println("The strings are identical.");\n }



###### BASIC - LOOPS ######

## faq - What is a loop
* basic_faq/what-is-a-loop
	- A loop is any repeating action. In Java, there are 4 loop types:\n\n1."for" loop \n2."for each" loop \n3."while" loop \n4."do ... while" loop\n\nYou can ask me about each one of the loops if you want to know more.

## faq - declare for-loop
* basic_faq/for-loop
	- A for-loop contains three parts.:\n\nfor(int i=0; i<10; i++){}\n\nThe first part declares a run variable and its start value, here an integer i being 0. The second part is a condition when to run the loop. The loop ends if the condition is not fulfilled anymore. The third part gets conducted after every iteration of the loop, increasing the integer i by 1. In this example, the loop will run ten times until it reaches i=10, which does not fulfill the condition anymore.

## faq - declare while-loop
* basic_faq/while-loop
	- A while loop, compared to a for-loop only contains the condition. It will run until the condition gets broken.\n\nwhile(i<20) {}\n\nThis loop would run until a variable i becomes 20 or higher.

## faq - declare do-while-loop
* basic_faq/difference-do-while
	- The difference between while- and do-while-loops is, that the do-while-loop proves its condition after running the loop. This means that it will run at least one time, even though the condition already gets broken before running it the first time.

## faq - what is a for-each loop
* basic_faq/for-each-loop
	- for-each loops are an alternative way to iterate over all elements of a list or an array. For more information on how to use it, have a look at that website: [For each loop in Java](https://www.geeksforgeeks.org/for-each-loop-in-java/)

## faq - what is a (non-existing) if-loop
* basic_faq/if-loop
	- There is no if-loop existing in any language. ifs are conditions, but no loops. You have mixed up to two important Java terms.

## faq - use infinite loop
* basic_faq/infinite-loop
	- You can get an infinite loop with while(true). Make sure to somehow break it.



###### BACIS - JAVA METHODS ######

## faq - What is a method
* basic_faq/what-is-a-method
	- A method is a block of code which only runs when it is called. You put code into functions which you would like to call several times such that you do not need to write the same code multiple times.

## faq - What is a function
* basic_faq/what-is-a-function
	- We mostly use the word 'method' for a function in Java. You should ask me about methods in Java.

## faq - What is a main method
* basic_faq/main-method
	- Java main method is the entry point of any java program. Without the main() method, JVM will not execute the program. Its syntax is:\n\n public static void main(String[] args).

## faq - Use a method
* basic_faq/use-a-method
	- A method must be declared within a class. It is defined with the name of the method, followed by parentheses (). You can create your own methods to perform certain actions like this:\n\npublic class MyClass {\nstatic void myMethod() {// code to be executed}\n}\n\nTo call a method in Java, write the method's name followed by two parentheses () and a semicolon:\n\nmyMethod();

## faq - call a method
* basic_faq/call-a-method
	- You can call a method by writing its name and filling the arguments inside the parentheses. A method adding to numbers together would be called like: add(5, 7); for the numbers 5 and 7.

## faq - Method parameters
* basic_faq/method-parameters
	- Information can be passed to methods as parameter. Parameters act as variables inside the method. Parameters are specified after the method name, inside the parentheses. You can add as many parameters as you want, just separate them with a comma.\n\nstatic void myMethod(String fname) {}\n\nThis method takes a String called fname as parameter.

## faq - Multiple method parameters
* basic_faq/multiple-parameters
	- You can have as many method parameters as you like:\n\nstatic void myMethod(String fname, int age) {}

## faq - void methods
* basic_faq/void
	- A void method is a method with no return. It never returns back any type of value.

## faq - Static methods
* basic_faq/static-methods
	- Static method is a method that is declared with a static keyword. The most common example of a static method is main( ) method.\n\nAny static member can be accessed before any objects of its class are created, and without reference to any object.

## faq - Static vs. public class methods
* basic_faq/static-public-difference
	- Static method means that it can be accessed without creating an object of the class, unlike public, which can only be accessed by objects.

## faq - Pass-by-reference
* basic_faq/pass-by-reference
	- All objects (e.g. strings) are passed by reference in Java compared to pass-by-value for primitive datatypes (e.g. int). This means that you do not copy the value, but the reference of the object in memory. If you then change the properties of one copy, all copies follow the change. Be aware!

## faq - Pass-by-value
* basic_faq/pass-by-value
	- All primitive datatypes (e.g. int, double) in java copy their values by value, which means that you only copy the value itself when using the equal sign. The variables are never directly connected in memory and therefore behave independent.





###### BASIC - OOP ######

## faq - What is OOP
* basic_faq/what-is-oop
	- OOP stands for Object-Oriented Programming.\n\nProcedural programming is about writing procedures or methods that perform operations on the data, while object-oriented programming is about creating objects that contain both data and methods. Classes and objects are the two main aspects of object-oriented programming.\n\nIf you want to know more, ask me 'what are the key concepts of OOP?.

## faq - Concepts of OOP
* basic_faq/oop-concepts
	- OOPs concepts include:\n\nInheritance\nEncapsulation\nPolymorphism\nAbstraction\nInterface\n\nYou can ask me about each one of them.

## faq - What is an object
* basic_faq/what-is-an-object
	- An object is one instance of a class in java. If a class is not static, you can generate as many objects of a class as possible.

## faq - What is a class
* basic_faq/what-is-a-class
	- A class is a user defined blueprint or prototype from which objects are created.  It represents the set of properties or methods that are common to all objects of one type. It is a basic unit of Object Oriented Programming.

## faq - What is a constructor
* basic_faq/constructor
	- A constructor is an initial function that gets called automatically if you create a new object of a class. It can for example preinitialise values of the class. For more information on how to use them, have a look for this wonderful manual on the Internet: [Constructors in Java](https://beginnersbook.com/2013/03/constructors-in-java/)

## faq - Class attributes
* basic_faq/class-attributes
	- Class attributes are variables within a class. They are also somethimes called fields.\n\npublic class MyClass {\nint x = 5;\nint y = 3;\n}

## faq - Class methods
* basic_faq/class-methods
	- Class methods methods are declared within a class, and that they are used to perform certain actions.\n\npublic class MyClass {\nstatic void sayHello() {System.out.println("Hello!");}\n}

## faq - How to access class attributes
* basic_faq/access-class-attributes
	- You can access attributes by creating an object of the class, and by using the dot syntax (.):\n\nMyClass.x

## faq - what is an enum(eration)
* basic_faq/enum
	- Enums are a special class of constants which you can use if you have a fixed size of constants to iterate over. One example would be the four directions: \n\nenum directions = {North, East, West, South};











###### ADVANCED FAQ ######




###### ADVANCED - RANDOM NUMBERS ######

## faq - Generate random numbers
* advanced_faq/random-number
	- You can use the Random class of java by importing it with the command: import java.util.Random;\n Afterwards, you can generate a dice by using the following command: new Random().nextInt(6)+1;\n Alternatively, you can use Math.random(); to get a random floating number between 0 and 1. 




###### ADVANCED - MATH FUNCTIONS ######

## faq - how to harvest the square root
* advanced_faq/square-root
	- You can calculate the square root with the standard math library by using Math.sqrt(number).

## faq - using trigometrical methods
* advanced_faq/sinus
	- You can calculate the sine and cosine with the standard math library by using Math.sin(number) and Math.cos(number).

## faq - calculate power of number
* advanced_faq/power
	- Math.pow(base, exp) calculates the power of a number.

## faq - square a number
* advanced_faq/square
	- Calculate a number with itself to square it.

## faq - min max integer value
* advanced_faq/min-max-integer
	- You can use Integer.MAX_VALUE and Integer.MIN_VALUE for that. It is a constant in the Integer class of java.lang package that stores the maximum or minimum possible value for any integer variable in Java.

## faq - what are floor and ceil
* advanced_faq/floor-ceil
	- Floor and Ceil are rounding functions. Both cut all digits after the comma away, where floor rounds down to the next integer and ceil will round up to the next integer. Example: double x = 1.73; Math.floor(x) returns 1 and Math.ceil(x) returns 2.

## faq - Round numbers
* advanced_faq/round-numbers
	- Math.round() rounds a double to the next integer.

## faq - use the GdP standard library
* advanced_faq/standard-library
	- Download the library from the website and import it with the import command.





###### ADVANCED - OTHER FUNCTIONS ######

## faq - how to draw a line
* advanced_faq/draw-line
	- StdDraw.line(x1, y1, x2, y2) draws a line from (x1, y1) to (x2, y2).

## faq - how to draw a rect
* advanced_faq/draw-rect
	- StdDraw.rect(x1, y1, width, height) draws a rect starting from (x1, y1) with the size (width, height).

## faq - how to draw a circle
* advanced_faq/draw-circle
	- StdDraw.circle(x, y, d) draws a circle with diameter d around the point (x,y).

## faq - get current time
* advanced_faq/get-Time
	- The command System.currentTimeMillis(); returns the time which has passed since 1.1.1970 in milliseconds (the so called unix time). Be aware that the return type is a long.

## faq - get input
* advanced_faq/input
	- You can use our GdP standard library provided by you. This contains the class StdIn with methods like StdIn.readDouble, StdInt.readInt or StdInt.readString handling the input and the types for you.

## faq - how to compile a java file
* advanced_faq/compile-java-file
	- Two compile a new java file "j" you type in the following command into your terminal: \njavac j.java. \n\nThis leads to a new file j.class. Afterwards you can run it with: \njava j 

## faq - run java from terminal
* advanced_faq/run-java-from-terminal
	- You can run a java file named j by typing the following command into the terminal: \njava j. \n\nMake sure you compiled the file before (using command "javac j.java").






###### ADVANCED - READ/WRITE FILES ######

## faq - how to read a file
* advanced_faq/read-file
	- There is no easy, but a lot of usable ways to read files in java. Here you have a website with good explained websites, solving all of your file reading problems: [Different ways reading text file Java](https://www.geeksforgeeks.org/different-ways-reading-text-file-java/)

## faq - how to write a file
* advanced_faq/write-to-file
	- There is no easy, but a lot of usable ways to write into files in java. Here you have a website with good explained websites, solving all of your file writing problems: [Java write to file](https://howtodoinjava.com/java/io/java-write-to-file/)





###### ADVANCED - EXEPTION HANDLING ######

## faq - how to use try and catch
* advanced_faq/try-catch
	- Try and catch is a nice functionally with which you can make your code reacting for exceptions. For more information, have a look for that tutorial here: [Try catch in Java](https://beginnersbook.com/2013/04/try-catch-in-java/)

## faq - how to throw exceptions
* advanced_faq/throw-exception
	- The operator 'throw' allows you to throw an exception. For more detailed information, have a look for that wonderful website: [Throw throws in Java](https://www.geeksforgeeks.org/throw-throws-java/)




###### ADVANCED - OOP CONCEPTS ######

## faq - Abstraction
* advanced_faq/abstract-classes
	- Abstract classes and methods are structures in Java from which you can inherat from. It does not include any method body and you must therefore implement it fully when you inherat from it. For more information on how to use them, have a look for this wonderful manual on the Internet: [Java Abstraction](https://www.tutorialspoint.com/java/java_abstraction.htm).

## faq - Polymirphism
* advanced_faq/polymorphy
	- Polymorphism is the ability of an object to take on many forms. You could for example programme a class called Animal which can have different appearances like Dog or Cat, having different behaviour. For a detailed overview on how to use polymorphy, have a look at this website: [Polymorhism in Java](https://www.tutorialspoint.com/java/java_polymorphism.htm).

## faq - Inheritance
* advanced_faq/inheritance
	- Inheritance means one class can extend to another class. So that the codes can be reused from one class to another class. The existing class is known as the Super class whereas the derived class is known as a sub class. Example: \n\nSuper class:\npublic class Manupulation(){}\n\nSub class:\npublic class Addition extends Manipulation(){}\n\nInheritance is only applicable to the public and protected members only. Private members can’t be inherited. Take a look here if you want to know more: https://[Java Inheritance](www.tutorialspoint.com/java/java_inheritance.htm).

## faq - Interfaces
* advanced_faq/interfaces
	- An interface is a completely "abstract class" that is used to group related methods with empty bodies. It is a template which has only method declarations and not the method implementation. For more information on how to create and use them, have a look for this wonderful manual on the Internet: [Java Interfaces](https://www.tutorialspoint.com/java/java_interfaces.htm).

## faq - Encapsulation
* advanced_faq/encapsulation
	- Purpose of Encapsulation:\n\nProtects the code from others.\nCode maintainability.\n\nLet's say we are declaring ‘a’ as an integer variable and it should not be negative.\n\npublic class Addition(){int a=5;}\n\nIf someone changes the exact variable as “a = -5” then it is bad. In order to overcome the problem we need to follow the steps below: \n\n1)We can make the variable private or protected.\n2)Use public accessor methods: get and set.\n\n Read more here: [Java Encapsulation](https://www.tutorialspoint.com/java/java_encapsulation.htm).




###### ADVANCED - RECURSION ######

## faq - what is recursion
* advanced_faq/recursion
	- You can use recursion if a method calls itself several times. There needs to be a condition when the recursion will end. Good examples for recursion are the calculation of factorial and fibonacci numbers.

## faq - calculate fibonacci
* advanced_faq/calculate-fibonacci
	- There are many amazing manuals for that, have a look for this wonderful explanation on the Internet: [Fibonacci series in Java using recursion](https://www.java67.com/2016/05/fibonacci-series-in-java-using-recursion.html)

## faq - calculate factorial
* advanced_faq/calculate-factorial
	- There are many amazing manuals for that, have a look for this wonderful explanation on the Internet: [Factorial recursion](https://www.programiz.com/java-programming/examples/factorial-recursion)





###### ADVANCED - SPECIAL KEYWORDS ######

## faq - what is super
* advanced_faq/super
	- The super operator refers to parent classes (super classes) in Java polymorphy. You can use the super operator to get the value or functionaly of a variable/method in the classes parent class.

## faq - what is this
* advanced_faq/this
	- This is a reference variable refering to the current object which it is inside. Example: if you write "this.a", the programme is providing you with the value of a inside the current object. The topic is complex but important, therefore we provide you with that amazing Internet tutorial: ['this' refenrence in Java](https://www.geeksforgeeks.org/this-reference-in-java/)

## faq - what does deprecated mean
* advanced_faq/deprecated
	- A deprecated method is a method which is not longer considered important. It got replaced by a new method inside the library and it is recommended to not use it anymore. In fact, you can formally use it anyway.




###### ADVANCED - UNIT TESTING ######

## faq - what is junit
* advanced_faq/junit
	- JUnit is a testing framework in Java. It can be used to automatically prove if your code breaks rules which you defined before. For more information, have a look for that tutorial here: [JUnit](https://www.tutorialspoint.com/junit/index.htm)





###### ADVANCED - SEARCHING AND SORTING #######

## faq - Sort Array
* advanced_faq/sort-array
	- An array named a can by sorted with the following function: Arrays.sort(a); Make sure to import java.util.Arrays.

## faq - Searching algoritms
* advanced_faq/search-array
	- Sorry, I can't help you with Searching Algorithms in Java yet. I'm still learning about it. Check this article: [Searching Algorithms] (https://www.geeksforgeeks.org/searching-algorithms/).


###### ADVANCED - METHOD OVERRRIDING AND OVERLOADING ######

## faq - Method Overloading
* advanced_faq/method-overloading
	- In Java, you are allowed to name two methods with the same name, using a different amount or different types of parameters. You could for example declare two add functions, one taking two integer arguments and one taking three double arguments, both adding together all numbers.\n\nYou can find some examples here: [Method Overloading](https://beginnersbook.com/2013/05/method-overloading/)

## faq - Method Overriding
* advanced_faq/method-overriding
	- Declaring a method in sub class which is already present in parent class is known as method overriding. Overriding is done so that a child class can give its own implementation to a method which is already provided by the parent class. In this case the method in parent class is called overridden method and the method in child class is called overriding method.\n\nYou can look at the examples here: [Method Overriding](https://beginnersbook.com/2014/01/method-overriding-in-java-with-example/)

## faq - Method Overriding
* advanced_faq/method-overriding-vs-overloading
	- One of the differences between overriding and overloading is:\n\nOverloading happens at compile-time while Overriding happens at runtime: The binding of overloaded method call to its definition has happens at compile-time however binding of overridden method call to its definition happens at runtime.\n\nTake a look here: [Difference between method overloading and overriding in Java](https://beginnersbook.com/2014/01/difference-between-method-overloading-and-overriding-in-java/)


###### ADVANCED - DATA STRUCTURES ######

## faq - Tree
* advanced_faq/tree
	- Tree is a data structure. Unfortunately I don't know much about trees yet. You can read about binary trees here: [Implementing a Binary Tree in Java](https://www.baeldung.com/java-binary-tree).

## faq - Heap
* advanced_faq/heap
	- Heap is a data structure. Unfortunately I don't know much about heaps yet. You can read about heaps here: 

## faq - Stack
* advanced_faq/stack
	- The Stack is a data structure.\n\nYou can think of a stack literally as a vertical stack of objects; when you add a new element, it gets stacked on top of the others.\n\nWhen you pull an element off the stack, it comes off the top. In other words, the last element you added to the stack is the first one to come back off.\n\nFor more details about this class, check [The Stack](https://www.tutorialspoint.com/java/java_stack_class.htm).

## faq - Queue
* advanced_faq/queue
	- Queue is a data structure. Unfortunately I don't know much about queues yet. Here are a few links where you can find about Queues: [Queue Data Structure](https://www.geeksforgeeks.org/queue-data-structure/), [Queue implementation in java](https://www.techiedelight.com/queue-implementation-in-java/)

## faq - Map
* advanced_faq/map
	- Map is a data structure. We can map a unique key to a specific value. It is a key/value pair. We can search a value, based on the key. Like the set, the map also uses the “equals ( )” method to determine whether two keys are the same or different. You can read more here [Map in Java](https://docs.oracle.com/javase/8/docs/api/java/util/Map.html).

## faq - Dictionary
* advanced_faq/dictionary
	- Dictionary is a data structure that represents a key-value relation. Given a key you can store values and when needed can retrieve the value back using its key. Thus, it is a list of key-value pair. Take a look here: [Dictionary Class in Java (java util)](https://www.geeksforgeeks.org/java-util-dictionary-class-java/)

## faq - hashcodes in java
* advanced_faq/hashing
	- HashCodes are fundamental in computer science. A hash is a value defining the content of an object or variable. You can use two hashcodes to compare if two objects are the same (if there was no hashcollision). The topic is complex but useful, therefore we provide you with that amazing Internet tutorial: [Java Hashcode](https://www.baeldung.com/java-hashcode)






##### ADVANCED - NOT SORTED YET... ######

## faq - Wrapper classes
* advanced_faq/wrapper-class
	- A Wrapper class is a class whose object wraps or contains primitive data types. When we create an object to a wrapper class, it contains a field and in this field, we can store primitive data types. In other words, we can wrap a primitive value into a wrapper class object. Take a look here: [Wrapper classes in Java](https://www.geeksforgeeks.org/wrapper-classes-java/)

## faq - what is a comparable
* advanced_faq/comparable
	- Comparable is an interface in java with which you can define how the equals function should work for your on classes. It therefore defined when two objects of a class are equal. For more information, have a look for that tutorial here: [Comparable interface in collection framework](https://www.javatpoint.com/Comparable-interface-in-collection-framework)

## faq - what are java collections?
* advanced_faq/collections
	- Java collections is a framework which proved a lot of usedful predefined datatypes such that ArrayList, Queue or Stack and all of its standard operations. For more information on how to use them, have a look for this wonderful manual on the Internet: [Collections in Java](https://www.javatpoint.com/collections-in-java)



## faq - Out of scope questions
* advanced_faq/out_of_scope
	- Sorry, I don't have an answer for that. I'm still learning 🙂.