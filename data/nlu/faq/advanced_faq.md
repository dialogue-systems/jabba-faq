###### ADVANCED - OOP CONCEPTS ######

## intent:advanced_faq/abstract-classes
- abstract classes
- class abstract
- abstract class
- what is an abstract class?
- what does abstract mean?
- abstrct classes
- What is an abstract java class?

## intent:advanced_faq/interfaces
- what are interfaces
- java interfaces
- implements other class
- interface
- interfaces in java
- whats an interface?
- what is a java interface
- what are interfaces in java?

## intent:advanced_faq/polymorphy
- What is polimorphism
- what is polymorphie
- What is polymorphie
- Give me an example of polymorphism
- What is polymorphism
- how does polymorphy work
- what does polymorphy mean?
- what is polymorphy?
- what is inheritance
- polymorphism
- polimorfi
- polimorphy

## intent:advanced_faq/inheritance
- What is inheritance
- What is inheritance?
- what is inheritance
- whats inheritance
- what is inhertiance
- what is inheritance
- what does inheritance mean
- inheritance
- inheritance in Java
- what does inheritance mean 
- How doe inheritance work?
- how does inheritance work?
- What is inheritance in Java?

## intent:advanced_faq/encapsulation
- What is encapsulation?
- How does encapsulation work
- whats encapsulation?
- what does encapsulation mean?
- What is encapsulation?
- what's encapsulation
- wncapsulation
- Encapsulation
- encapsulation
- encapsulation in Java
- What does encapsulation mean?
- what does encapsulation mean



###### RANDOM NUMBERS ######

## intent:advanced_faq/random-number
- How to generate a random number in Java?
- random number
- give me a random number
- how can I get a random number?
- roll dice
- flip a coin
- Math.random
- random number between 1 and 10
- random number between 
- generate random numbers


##### ADVANCED - MATH FUNCTIONS ######

## intent:advanced_faq/square-root
- how can I get the square root of a value?
- square root
- calculate root
- Math.sqrt
- calc sqrt
- how do i get the square root of an int
- how do i get the square root of a float

## intent:advanced_faq/sinus
- how can I get the sinus of a value?
- how can I get the cosinus of a value?
- how can I get the sine of a value?
- how can I get the cosine of a value?
- trigonometrical functions
- sinus cosinus
- sine cosine
- sin cos

## intent:advanced_faq/power
- how can I get the power of a number?
- how to get the power of a number
- power of number
- number power
- Potency of a number
- cube of number
- how do i get the power of an int
- how do i get the power of a float

## intent:advanced_faq/square
- how can I get the square of a number?
- how to write the square of a number
- square of number
- square of an int
- square number
- how do i get the square of an int
- how do i get the square of a float
- square of a number

## intent:advanced_faq/min-max-integer
- How to get max int in java
- maximum integer value in java
- how to declare a maximum possible integer
- How to use max int in java
- How to get min int in java
- minimum integer value in java
- how to declare a minimum possible integer
- How to use min int in java
- how to initialise a maximum integer
- how to initialise a minimum integer

## intent:advanced_faq/floor-ceil
- what is the difference between floor and ceil?
- floor ceil
- ceil and floor
- Math.ceil
- Math.floor
- difference between floor and ceil
- difference between cail and floor

## intent:advanced_faq/round-numbers
- how can I round comma numbers?
- how to round floating numbers?
- round double to integer
- how to remove comma in double
- how to print only 2 digits?
- restrict number of digits?
- round numbers
- rounding numbers

## intent:advanced_faq/standard-library
- standard library gdp
- gdp library
- io library
- library input output
- how to use java standard library
- use standard library
- use standard library gdp



###### ADVANCED - OTHER FUNCTIONS ######

## intent:advanced_faq/draw-line
- how to draw a line
- draw line graphics
- draw line
- line from point to point
- io line
- grapical interface line
- gui line
- how can i draw a line in java
- draw line in java
- darw line in java

## intent:advanced_faq/draw-rect
- how to draw a rectangle
- draw rectangle graphics
- draw rect
- io rectangle
- grapical interface rect
- gui rectangle
- how can i draw a rectangle in java
- how can i draw a rectagle
- draw rectangle in java

## intent:advanced_faq/draw-circle
- how can I draw a circle
- draw circle graphics
- draw circle
- io circle
- grapical interface circle
- gui circle
- how can i draw a circle in java
- how can i draw a circel
- draw circle in java
- how to draw a circle
- how to draw a cirkle

## intent:advanced_faq/get-Time
- return current time
- get time in java
- clock time in java
- time in milliseconds
- current time
- how can i get current time in java
- get curent time
- get time

## intent:advanced_faq/input
- how does input work?
- how to hand in numbers to java?
- start programme with parameter
- read in a number
- use input parameters

## intent:advanced_faq/compile-java-file
- how can I compile a java file?
- how to compile a java file
- how does compilation of a java file work
- java file compilation
- compile java file
- javac file.java

## intent:advanced_faq/run-java-from-terminal
- how is it possible to run a java file from terminal?
- run java from terminal
- terminal java
- execute java file
- execute java file in terminal



###### ADVANCED - READ/WRITE FILES ######

## intent:advanced_faq/read-file
- how can I read a text file in java?
- txt file input
- input text file
- reading txt file
- read text file 
- read text file in java
- read characters from text file

## intent:advanced_faq/write-to-file
- how can I write to a text file in java?
- txt file output
- output text file
- write data into text file
- write data into file
- how to write data into a text file
- how to write strings into a .txt file



###### ADVANCED - EXEPTION HANDLING ######

## intent:advanced_faq/try-catch
- finally block
- try catch
- try catch finally
- try and catch
- catch an exception
- how to catch an exception
- how can i catch an exception
- how do i implement try catch

## intent:advanced_faq/throw-exception
- how to throw an exception
- put exception into terminal
- throw exception
- throwing exceptions



###### ADVANCED - RECURSION ######

## intent:advanced_faq/recursion
- how does recursion work?
- what is recursion?
- method calling itself
- selfcalling functions
- recursive methods?
- how to use recursion
- what does recursion mean
- what is a self calling function
- how do you call a self calling function

## intent:advanced_faq/calculate-factorial
- calculate factorial
- factorial
- get faculty of number
- faculty number
- factorial number
- how to calcualte the faculty of number
- how to calcualte factorial
- calculate faktorial
- how to calkualte factorial

## intent:advanced_faq/calculate-fibonacci
- get fibonacci numbers
- fib number
- fibonacci number
- fibounachi number
- how to calculate fib
- how to calculate fibonacci
- how to code fibonacci



###### ADVANCED - SPECIAL KEYWORDS ######

## intent:advanced_faq/super
- what is super?
- when to use super()?
- constructor super
- what does super do?
- how do i use super?
- what does super mean
- when can i use super
- when does it make sence to use super?
- what does the super operator do?
- how to use super operator

## intent:advanced_faq/this
- what does "this" mean?
- when to use this?
- this.variable
- what does the "this" variable do?
- how can i use the "this" variable

## intent:advanced_faq/deprecated
- what is deprecated?
- deprecated
- my method is deprecated
- deprecated methods
- depriceted method
- what does deprecated mean?
- whats a deprecated method
- What does it mean for a method to be deprecated?
- What does depriceted method mean
- what does it mean if a method is deprecated



###### ADVANCED - UNIT TESTING ######

## intent:advanced_faq/junit
- what is junit
- junit
- testing frameworks
- test correctness



###### ADVANCED - SEARCHING AND SORTING #######

## intent:advanced_faq/sort-array
- how can I sort in Java?
- arange arrays after values
- sort array
- sort numbers
- bubblesort
- sort algorithm
- algorithm for sorting
- sorting
- Sort ana array
- How to sort an array?
- How to sort elements in an array?

## intent:advanced_faq/search-array
- Searching algorithms
- search algorithms
- Searching 
- search algorithms in java



###### ADVANCED - METHOD OVERLOADING AND OVERRIDING ######

## intent:advanced_faq/method-overloading
- how to overload method?
- how to overlod function
- how do I overload a function
- what does overloading a function mean
- how to use method with different parameters?
- different attributes for same method name
- overload different attributes
- overload function

## intent:advanced_faq/method-overriding
- what is method overriding?
- what is method overwriting
- What is meant by Method Overriding?
- method overriding
- method overloading
- What is method overriding?
- Method overwritind
- method overwriting

## intent: advanced_faq/method-overriding-vs-overloading
- Overloading vs Overriding in Java
- overloading versus overriding
- What's the difference between overriding and overloading?
- method overriding vs overloading
- overriding overloading




###### ADVANCED - DATA STRUCTURES ######

## intent:advanced_faq/tree
- what is a tree?
- what is a tree in Java?
- explain me about trees
- tree data structure
- What is a tree?
- what is a tree in Java?
- What is a Tree
- trees in Java
- Java tree
- What is a tree
- What is a binary tree
- what is avl tree

## intent:advanced_faq/heap
- What is a heap?
- what is a heap
- what is a heap
- Whats a heap
- What is a heap?
- whats a heap
- Heap
- heaps
- Explain me what a heap is
- explain me heaps
- Explain me heaps

## intent:advanced_faq/stack
- What is a stack?
- what is a stack
- what is a stack
- Whats a stack
- What is a stack?
- whats a stack
- a stack
- Stack
- Explain me what a stack is
- explain me stack
- Explain me stack

## intent:advanced_faq/queue
- What is a queue?
- what is a queue
- what is a queue
- Whats a queue
- What is a queue?
- whats a queue
- queeu
- Queue
- queues
- Explain me what a queue is
- explain me queues
- Explain me queue

## intent:advanced_faq/map
- what is a map?
- What is a map?
- Map in Java
- what is a map in Java?
- Map data structure
- what is a map?
- How to use a map?
- how to map values to keys?

## intent:advanced_faq/dictionary
- what is a dictionary
- does java have dictionaries?
- what is a dictionary?
- How to use dictionaries?
- What is a dictionary in Java?
- what is adictionary
- dictionary

## intent:advanced_faq/hashing
- what is hashing
- what does hashing mean?
- hashes
- what are hashcodes
- hashCode


##### ADVANCED - NOT SORTED YET... ######

## intent:advanced_faq/wrapper-class
- What is a wrapper class
- what is a wrapper class
- what is a wrapper
- when to use wrapper class over primitive datatypes?
- When to use wrapper class and primitive type
- When to use wrapper classes
- What does wrapper clas mean?
- why should i use a wrapper class
- Wrapper classes
- wrapper classes
- wrapper class

## intent:advanced_faq/collections
- what are collections?
- collections
- stacks
- queues

## intent:advanced_faq/comparable
- comparable interface
- what is a comparable interface
- implements comparable
- compare objects of classes
- class comparation
- comprare objekts








##### OUT OF SCOPE - EVERYTHING WHAT OUR FAQ CAN NOT HANDLE YET ######

## intent:advanced_faq/out_of_scope
- what is dynamic casting
- dynamic vs static casting
- what does greedy mean?
- what is a greedy algorithm
- naive algorithm
- greedy algorithms
- What is a graph
- graph algorithms
- find shortest path
- djekstra
- what is a node
- what is a leaf
- How does Java enable high performance?
- Graph theory
- graph theory
- Complexity
- What is complexity of this algorithm
- complexity theory
- what is spring
- string builder
- string buffer
- Buffer
- stream
- What is meant by Ordered and Sorted in collections?
- Explain the different lists available in the collection.
- Explain about Set and their types in a collection.
- What is a Thread?
- what is a thread
- threads
- How do you make a thread in Java?
- How to stop a thread in java?
- Explain about join () method.
- What does the yield method of the Thread class do?
- Explain about wait () method
- Difference between notify() method and notifyAll() method in Java
- Runnable interface
- Thread class in Java
- start() and run() method of thread class
- multithreading
- synchronization
- serialization
- Serialization and Deserialization in Java
- singleton class
- What's the base class in Java from which all classes are derived?
- What are Java Packages? What's the significance of packages?
- generics
- generic
- Java Generic methods
- java packages
- Packages
- creating a package
- sending email
- hr
- gdp
- Regular expressions
- regular expressions
- how to use regular expressions?
- how to use regex?
- regex
- applet
- api
- garbage collector
- what is a garbage collector
- Garbage collector
- html
- what is html
- alphabet


###
###
### Missing intents:
### Dynamic vs. static binding
### Data structures - trees, heaps...
### Algorithms - Greedy, Dynamic programming, Sorting and Search, 
### Graph algorithms like shortest path etc.
### Feel free to extend this file and functionality
### This intent 'advanced_faq' is always followed by stackoverflow result