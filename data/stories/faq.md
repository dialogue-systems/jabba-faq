## Greeting
* greetings.greet
    - utter_greet
    - utter_about_me

## Basic FAQ - Basic FAQ is single turn
* basic_faq
    - respond_basic_faq

## Advanced FAQ - followed by stackoverflow - affirm
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* confirmations.affirm OR confirmations.maybe
    - action_stackoverflow

## Advanced FAQ - followed by stackoverflow - deny
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* confirmations.deny
    - utter_glad_to_help

## Advanced FAQ - Two advanced FAQ question after each other
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help

## Advanced FAQ - Basic FAQ interruption
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* basic_faq
    - respond_basic_faq

## FAQ - Stackoverflow intent (questions that we don't have a predefined response for)
* stackoverflow
    - action_stackoverflow

## Chitchat
* chitchat
    - respond_chitchat

## Goodbye
* greetings.goodbye
    - utter_goodbye

## Random confirmation
* confirmations.affirm OR confirmations.deny OR confirmations.maybe
    - utter_random_confirmation