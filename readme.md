# Jabba FAQ

Jabba is a chatbot build using Rasa Open Source framework. We collected data for most frequently asked questions on Java programming and created a FAQ chatbot. Jabba-FAQ is just a part of the complete project called Jabba.

Jabba-FAQ can answer questions like 'What are the key concepts of OOP?', 'How to declare an array?' or 'What is the syntax of the main method in Java?'.

We prepared a dataset of question-answer pairs and we want to make it public for others to use and extend it further.

# Run this project

#### Dependencies:
* Python 3.7
* Rasa 1.10

#### 1. Install dependencies

Create a virtual environment (recommended)

```bash
python3 -m venv env
source env/bin/activate
```

The only requirement to run Jabba-FAQ is Rasa 1.10

```bash
pip install -r requirements.txt 
```

#### 2. Train the model

```bash
rasa train
```

#### 3. Speak to the bot in the shell

```bash
rasa shell
```