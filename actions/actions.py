import requests
import json
from rasa_sdk import Action, Tracker
from rasa_sdk.events import EventType
from typing import Text, List, Optional, Union, Any, Dict

# imports for fallback action
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import UserUtteranceReverted


class ActionStackoverflow(Action):

    def name(self) -> Text:
        return "action_stackoverflow"
    
    # adapted from rasa-demo chatbot
    def get_last_event_for(self, tracker, event_type: Text, skip: int = 0) -> Optional[EventType]:
        skipped = 0
        for e in reversed(tracker.events):
            if e.get("event") == event_type:
                skipped += 1
                if skipped > skip:
                    return e
        return None

    def run(self, dispatcher, tracker, domain) -> List[EventType]:
        # if ActionStackoverflow isn't invoked directly via intent (but as a supplement after unsatisfactory bot answer)
        # we extract the user_question from previous event.
        if not tracker.latest_message['intent'].get('name') == 'stackoverflow':
            user_event_with_query = self.get_last_event_for(tracker, "user", skip=1)
            if user_event_with_query:
                user_question = user_event_with_query.get("text")
            else:
                dispatcher.utter_message(text="Sorry, I can't answer your question.")
                return []
        else:
            user_question = tracker.latest_message.get('text')

        res = requests.get(

            'https://api.stackexchange.com/2.2/search/advanced?'

            f'order=desc&sort=relevance&q={user_question}&tagged=Java&site=stackoverflow')

        res = res.json()

        if res.get('items'):
            link = res.get('items')[0]['link']
            title = res.get('items')[0]['title']

            dispatcher.utter_message("Here's what I found on StackOverflow:")
            dispatcher.utter_message(f'[{title}]({link})')
        else:
            dispatcher.utter_message("I did not find any matching questions on [StackOverflow](https://stackoverflow.com)")
            dispatcher.utter_message('I recommend you post your question there.')

        return []


class ActionDefaultFallback(Action):
    def name(self) -> Text:
        return "action_default_fallback"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[EventType]:
    
        dispatcher.utter_message(template="utter_fallback")
        return [UserUtteranceReverted()]
